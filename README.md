# The Coin Analogy
The concept of spinors in particle physics can be hard to wrap your head around. Here is an analogy that can help.
Before we begin, find yourself a pair of identical coins, preferably with a head on them, but anything to distinguish orientation will do.

## What's the deal with spin?
The fundamental particles of the standard model can be thought of as little balls spinning around, except that they 
are not *REALLY* little balls, and they are not *REALLY* spinning.

Sorry, come again?

That's right, the usual explanations leave a little to be desired!
Before we even begin to talk about spinors, you'll need to at least have a grasp on what we mean by spin.

I recommend watching the first 8 minutes and 50 seconds of this video from the excellent PBS Space Time to get you up to speed: [PBS Space Time - Electrons DO NOT Spin](https://www.youtube.com/watch?v=pWlk1gLkF2Y)

## Spinors
Now that we've got the idea of spin down, it's time to talk about spinors.
It turns out, the fundamental particles can be split into three groups, relating to their spin.

* Spin $`0`$ &mdash; the Higgs
* Spin $`1\over 2`$ &mdash; the Fermions (matter particles)
* Spin $`1`$ &mdash; the Gauge Bosons (force-carrying particles)

Spin $`0`$ is easy enough &mdash; The Higgs simply doesn't spin.

Spin $`1`$ isn't too bad either; spin behaves like you are used to from classical mechanics.

Spin $`1\over 2`$ on the other hand, is where things begin to get weird. The Spin $`1\over 2`$ particles take two full
rotations (i.e. 720 degrees) to return to their initial state.

Now isn't that strange? Yes indeed, the stage has been set. Let us now turn our attention to our analogy!

## Setup
Begin by placing two coins side-by-side on a table, heads-up, such that the edges are touching and the heads are pointing the same way:

![coins](coins.png)

## The Trick
Now, press the coin in the center down with your finger so it doesn't move, and carefully roll the coin on the right around the other coin (try your best not to let it slip).

![rotation](rotation.png)

What we observe is that the head of the rotating coin is back to pointing the same direction it started at once it is on the left side of the reference coin.

In other words, it has completed one full 360-degree rotation. It takes a 720-degree rotation of the outer coin to get back to the starting position!

Now, you may still be confused. In our analogy, the coin is not just rotating in place, it is also moving around a reference point.
When the coin has rotated 360-degrees, it is in a different place on the table. What is different about a particle after rotating 360-degrees?

Well, I'm glad you asked! Our table represents the complex plane. Our reference coin being held in place corresponds to the origin. The position of the outer coin on the table represents the phase shift of the particle's wavefunction!

![phase](phase.png)

That's right! Rotating a fermion shifts the particle's wave-function; it takes two full rotations to return to the original rotation AND phase; and the relationship between rotation and phase is precisely as shown by the coins on the table. This is what spinors are!

In the example illustration, the coin was rotated up the hill to $`i`$ and back down the other side to $`-1`$. You might have noticed that we could have rotated the other way, down to $`-i`$ and back up the other side to $`-1`$.

What you've just discovered is particle chirality! The particle rotating from $`1`$ to $`-1`$ by route of $`i`$ is said to be right-handed. On the other hand, the particle rotating from $`1`$ to $`-1`$ by route of $`-i`$ is left-handed!

## Conclusion
By playing with a couple coins on a table, we've learned about spinors, the relationship between spin and phase, and chirality. So, next time you meet a beggar on the street, spare him some change &mdash; it just might teach him physics!
